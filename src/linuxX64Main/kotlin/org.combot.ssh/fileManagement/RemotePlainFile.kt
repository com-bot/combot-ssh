package org.combot.ssh.fileManagement

import kotlinx.cinterop.*
import libssh.*
import org.combot.core.fileManagement.File
import org.combot.core.fileManagement.PlainFile
import org.combot.core.fileManagement.authorization.FilePermissions
import org.combot.core.fileManagement.authorization.generateMode
import org.combot.ssh.IoException
import org.combot.ssh.SshClient
import platform.posix.*

@ExperimentalUnsignedTypes
public actual class RemotePlainFile actual constructor(
    private val sshClient: SshClient,
    override val parentDirPath: String,
    override val name: String,
    override val isBinaryFile: Boolean
) : SftpFile, PlainFile {
    override suspend fun isValid(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            val file = openSftpFile(filePath = createFilePath(parentDirPath, name),
                sftpSession = sshClient.sftpSession)
            sftp_close(file)
            true
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun size(): Long {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1L
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.size?.toLong() ?: -1L
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun appendBinary(vararg data: Byte): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun appendText(vararg data: String): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            var tmpResult = true
            val file = openSftpFile(filePath = createFilePath(parentDirPath, name), sftpSession = sshClient.sftpSession,
                accessType = O_WRONLY or O_APPEND)
            for (item in data) {
                val bytesWritten = sftp_write(file = file, buf = item.cstr, count = item.cstr.size.toULong())
                if (bytesWritten < 0) {
                    tmpResult = false
                    break
                }
            }
            sftp_close(file)
            tmpResult
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun readAllBinary(): Pair<Boolean, Array<Byte>> {
        TODO("Not yet implemented")
    }

    override suspend fun readAllText(): Pair<Boolean, Array<String>> {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var data = emptyArray<String>()
        val readSuccessful = try {
            val file = openSftpFile(filePath = createFilePath(parentDirPath, name), sftpSession = sshClient.sftpSession)
            val (tmpSuccess, newData) = file.readAllText()
            data = newData
            sftp_close(file)
            tmpSuccess
        } catch (ex: IoException) {
            false
        }
        return readSuccessful to data
    }

    override suspend fun readBinary(size: ULong): Pair<Boolean, Array<Byte>> {
        TODO("Not yet implemented")
    }

    override suspend fun readText(size: ULong): Pair<Boolean, String> {
        TODO("Not yet implemented")
    }

    override suspend fun writeBinary(vararg data: Byte): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun writeText(vararg data: String): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            var tmpResult = true
            val file = openSftpFile(filePath = createFilePath(parentDirPath, name), sftpSession = sshClient.sftpSession,
                accessType = O_WRONLY or O_TRUNC)
            for (item in data) {
                val bytesWritten = sftp_write(file = file, buf = item.cstr, count = item.cstr.size.toULong())
                if (bytesWritten < 0) {
                    tmpResult = false
                    break
                }
            }
            sftp_close(file)
            tmpResult
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun changePermissions(permissions: FilePermissions): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return sftp_chmod(sftp = sshClient.sftpSession, file = createFilePath(parentDirPath, name),
            mode = permissions.generateMode()) == SSH_OK
    }

    override suspend fun copy(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun changeOwnership(userId: UInt, groupId: UInt): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return sftp_chown(file = createFilePath(parentDirPath, name), owner = userId, group = groupId,
            sftp = sshClient.sftpSession) == SSH_OK
    }

    override suspend fun create(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            val file = openSftpFile(
                filePath = createFilePath(parentDirPath, name),
                sftpSession = sshClient.sftpSession,
                accessType = O_EXCL or O_CREAT
            )
            sftp_close(file)
            changePermissions(defaultFilePermissions())
            true
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun delete(): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun fetchGroup(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.group?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchUser(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.owner?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun move(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun fetchUserId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.uid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchGroupId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.gid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }
}
