@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.ssh.fileManagement

import org.combot.core.fileManagement.authorization.FilePermissionType
import org.combot.core.fileManagement.authorization.FilePermissions

internal fun createFilePath(parentDirPath: String, name: String) =
    "${if (parentDirPath.isNotEmpty()) "$parentDirPath/" else ""}$name"

internal fun defaultFilePermissions() = FilePermissions(
    owner = FilePermissionType.READ_WRITE,
    group = FilePermissionType.READ_WRITE,
    other = FilePermissionType.NONE
)

internal fun defaultDirectoryPermissions() = FilePermissions(
    owner = FilePermissionType.READ_WRITE_EXEC,
    group = FilePermissionType.READ_WRITE_EXEC,
    other = FilePermissionType.NONE
)
