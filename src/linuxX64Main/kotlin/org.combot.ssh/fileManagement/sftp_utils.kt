@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.combot.ssh.fileManagement

import kotlinx.cinterop.*
import libssh.*
import org.combot.core.fileManagement.authorization.FilePermissions
import org.combot.core.fileManagement.authorization.generateMode
import org.combot.ssh.IoException
import platform.posix.O_RDONLY

internal fun openSftpFile(
    filePath: String,
    sftpSession: sftp_session?,
    permissions: FilePermissions = defaultFilePermissions(),
    accessType: Int = O_RDONLY
) = sftp_open(
    session = sftpSession,
    file = filePath,
    accesstype = accessType,
    mode = permissions.generateMode()
) ?: throw IoException("Cannot open file")

internal fun openSftpDirectory(filePath: String, sftpSession: sftp_session?) =
    sftp_opendir(sftpSession, filePath) ?: throw IoException("Cannot open directory")

internal fun CPointer<sftp_file_struct>.readAllText(): Pair<Boolean, Array<String>> {
    val tmp = mutableListOf<String>()
    // Read in 16 KB chunks.
    val bufferSize = 16384
    val buffer = ByteArray(bufferSize)
    var bytesRead: Long
    var successfulRead = true
    buffer.usePinned { pinned ->
        do {
            bytesRead = sftp_read(file = this, buf = pinned.addressOf(0), count = bufferSize.toULong())
            // If there is data in the buffer then add it to tmp.
            if (bytesRead > 0) tmp += buffer.map { i -> i.toChar() }.toString()
            if (bytesRead < 0) {
                successfulRead = false
                break
            }
        } while (bytesRead > 0)
    }
    return successfulRead to tmp.joinToString().split("\n").toTypedArray()
}
