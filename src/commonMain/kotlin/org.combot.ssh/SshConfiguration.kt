package org.combot.ssh

@Suppress("EXPERIMENTAL_API_USAGE")
/** Contains SSH configuration information. */
public data class SshConfiguration(
    val user: String,
    val host: String,
    val port: UInt,
    val publicKey: String = "",
    val privateKey: String = ""
)
