package org.combot.ssh

import libssh.*
import org.combot.core.ProgramRunner

@ExperimentalUnsignedTypes
public actual class RemoteProgramRunner actual constructor(
    private val sshClient: SshClient,
    public override val programName: String,
    public override val workingDir: String,
    public override val programDir: String,
    public override val programArgs: Array<String>
) : ProgramRunner {
    override suspend fun changeEnvironmentVariable(name: String, value: String) {
        require(!sshClient.isClosed) { "The RemoteProgramRunner instance must be open" }
        val channel = channel_new(sshClient.sshSession) ?: throw IoException("Cannot create channel")
        ssh_channel_request_env(channel = channel, name = name, value = value)
    }

    override suspend fun fetchProcessId(): Int {
        val (data, _) = runCommand(cmd = "pgrep $programName")
        return try {
            if (data.isNotEmpty()) data.first().toInt() else -1
        } catch (ex: NumberFormatException) {
            -1
        }
    }

    override suspend fun isRunning(): Boolean = fetchProcessId() > -1

    override suspend fun runInBackground() {
        runCommand(0u, createCommand())
    }

    private fun createCommand() = buildString {
        if (programDir.isNotEmpty()) append("$programDir/")
        append(programName)
        programArgs.forEach { a -> append(" $a") }
    }

    override suspend fun runInForeground(timeout: UInt): Pair<Array<String>, Int> = runCommand(timeout, createCommand())

    private fun runCommand(timeout: UInt = 8u, cmd: String): Pair<Array<String>, Int> {
        // TODO: Apply timeout.
        require(!sshClient.isClosed) { "The RemoteProgramRunner instance must be open" }
        val channel = channel_new(sshClient.sshSession) ?: throw IoException("Cannot create channel")
        var rc = channel_open_session(channel)
        if (rc != SSH_OK) channel.handleSessionError()
        rc = ssh_channel_request_exec(channel, cmd)
        if (rc != SSH_OK) channel.handleCommandExecError()
        val result = channel.readAll()
        channel.cleanup()
        return result to rc
    }

    override suspend fun terminate() {
        runCommand(cmd = "pkill $programName")
    }
}
