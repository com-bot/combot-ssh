package org.combot.ssh.fileManagement

import org.combot.core.fileManagement.PlainFile
import org.combot.ssh.SshClient

/**
 * Represents a file that is managed via SFTP.
 * @param sshClient The SSH client to use.
 * @param parentDirPath The path to the parent directory.
 * @param name The name of the file.
 * @param isBinaryFile If *true* then this file is a binary file. However if *false* then this file is a text file.
 */
@ExperimentalUnsignedTypes
public expect class RemotePlainFile(
    sshClient: SshClient,
    parentDirPath: String,
    name: String,
    isBinaryFile: Boolean = false
) : SftpFile, PlainFile
