package org.combot.ssh

public interface Closable {
    public val isClosed: Boolean

    public fun close()
}
