package org.combot.ssh

internal val Boolean.intValue: Int
    get() = if (this) 1 else 0
