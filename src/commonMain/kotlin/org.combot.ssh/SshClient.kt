package org.combot.ssh

@ExperimentalUnsignedTypes
/**
 * Represents a SSH client.
 * @param host The host name or IP address of the SSH server.
 * @param port Port number of the SSH server.
 * @param logVerbosity The logging behaviour to use.
 */
public expect class SshClient(
    host: String,
    port: UInt,
    logVerbosity: SshLogVerbosity = SshLogVerbosity.NO_LOG
) : Closable {
    /** Checks to see if there is a connection to the SSH server. */
    public val isConnected: Boolean

    /** Verifies that the host is known. */
    public fun verifyKnownHost()

    /**
     * Connects to the SSH server. For automatic authentication just leave all parameters to their defaults.
     * @param user The user to use for authentication.
     * @param publicKey The path to the public key file.
     * @param privateKey The path to the private key file.
     */
    public fun connect(user: String = "", publicKey: String = "", privateKey: String = "")

    /** Disconnects from the SSH server (if connected). */
    public fun disconnect()
}
