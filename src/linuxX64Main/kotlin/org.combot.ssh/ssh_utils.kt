@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.combot.ssh

import kotlinx.cinterop.*
import libssh.*

internal fun ssh_channel.cleanup() {
    ssh_channel_send_eof(this)
    ssh_channel_close(this)
    ssh_channel_free(this)
}

internal fun ssh_channel.handleCommandExecError() {
    cleanup()
    throw IoException("Cannot execute command via SSH")
}

internal fun ssh_channel.readAll(): Array<String> = memScoped {
    val buffer = alloc<ByteVar>()
    val tmpResult = StringBuilder()
    var totalBytes = ssh_channel_read(channel = this@readAll, dest = buffer.ptr, count = sizeOf<ByteVar>().toUInt(),
        is_stderr = false.intValue)
    while (totalBytes > 0) {
        tmpResult.append(buffer.value.toChar())
        totalBytes = ssh_channel_read(channel = this@readAll, dest = buffer.ptr, count = sizeOf<ByteVar>().toUInt(),
            is_stderr = false.intValue)
    }
    tmpResult.split("\n").toTypedArray()
}

internal fun ssh_channel.handleSessionError() {
    cleanup()
    throw IoException("Cannot open session with channel")
}
