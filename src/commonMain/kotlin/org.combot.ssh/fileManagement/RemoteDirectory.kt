package org.combot.ssh.fileManagement

import org.combot.ssh.SshClient

@Suppress("EXPERIMENTAL_API_USAGE")
/**
 * Represents a directory that is managed via SFTP.
 * @param sshClient The SSH client to use.
 * @param parentDirPath The path to the parent directory.
 * @param name The name of the directory.
 */
public expect class RemoteDirectory(sshClient: SshClient, parentDirPath: String, name: String) : SftpDirectory
