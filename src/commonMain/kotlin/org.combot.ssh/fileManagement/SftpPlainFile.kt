package org.combot.ssh.fileManagement

import org.combot.core.fileManagement.PlainFile

/** Represents a SFTP text or binary file. */
@Suppress("EXPERIMENTAL_API_USAGE")
public interface SftpPlainFile : SftpFile, PlainFile
