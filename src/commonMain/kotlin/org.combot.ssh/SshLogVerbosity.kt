package org.combot.ssh

@ExperimentalUnsignedTypes
public enum class SshLogVerbosity(public val value: UInt) {
    NO_LOG(0u),
    WARNING(1u),
    PROTOCOL(2u),
    PACKET(3u),
    FUNCTIONS(4u)
}
