package org.combot.ssh

import kotlinx.cinterop.*
import libssh.*
import platform.posix.getenv

@ExperimentalUnsignedTypes
public actual class SshClient actual constructor(
    public val host: String,
    public val port: UInt,
    public val logVerbosity: SshLogVerbosity
) : Closable {
    internal val sshSession: ssh_session?
        get() = _sshSession
    private var _sshSession: ssh_session? = createSshSession()
    internal val sftpSession: sftp_session?
        get() = _sftpSession
    private var _sftpSession: sftp_session? = null
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    public actual val isConnected: Boolean
        get() = if (_isClosed) false else ssh_is_connected(_sshSession) == true.intValue

    private fun createSshSession() = memScoped {
        val result = ssh_new() ?: throw IoException("Cannot create new SSH session")
        val tmpPort = alloc<IntVar>()
        val verbosity = alloc<IntVar>()
        tmpPort.value = port.toInt()
        verbosity.value = logVerbosity.value.toInt()
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_HOST, value = host.cstr)
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_PORT, value = tmpPort.ptr)
        ssh_options_set(session = result, type = ssh_options_e.SSH_OPTIONS_LOG_VERBOSITY, value = verbosity.ptr)
        result
    }

    override fun close() {
        if (!_isClosed) {
            disconnect()
            if (_sshSession != null) ssh_free(_sshSession)
            _sshSession = null
            _isClosed = true
        }
    }

    public actual fun verifyKnownHost(): Unit = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        val publicKey = alloc<ssh_keyVar>()
        var rc = ssh_get_server_publickey(_sshSession, publicKey.ptr)
        val hash = alloc<UByteVar>()
        val len = alloc<ULongVar>()
        if (rc < 0) throw IoException("Cannot get public key")
        rc = ssh_get_publickey_hash(
            key = publicKey.value,
            type = ssh_publickey_hash_type.SSH_PUBLICKEY_HASH_SHA256,
            hash = cValuesOf(hash.ptr),
            len.ptr
        )
        ssh_key_free(publicKey.value)
        if (rc < 0) throw IoException("Cannot get hash for public key")
        checkPublicKeyIsKnown()
    }

    private fun checkPublicKeyIsKnown() {
        when (ssh_session_is_known_server(_sshSession)) {
            SSH_KNOWN_HOSTS_OK -> println("Public key is known")
            else -> throw IllegalStateException("Unknown public key")
        }
    }

    public actual fun connect(user: String, publicKey: String, privateKey: String) {
        require(!_isClosed) { "The SshClient instance must be open" }
        val rc = ssh_connect(_sshSession)
        if (rc != SSH_OK) {
            throw IoException("Cannot connect to SSH server: ${ssh_get_error(_sshSession)?.toKString()}")
        }
        verifyKnownHost()
        if (user.isNotEmpty() && publicKey.isNotEmpty() && privateKey.isNotEmpty()) {
            authenticateManually(user = user, publicKey = publicKey, privateKey = privateKey)
        } else {
            authenticateAutomatically()
        }
    }

    private fun authenticateAutomatically() = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        when (ssh_userauth_publickey_auto(session = _sshSession, username = null, passphrase = null)) {
            SSH_AUTH_ERROR -> handleAuthenticationFailure(_sshSession, "unknown error")
            SSH_AUTH_DENIED -> handleAuthenticationFailure(_sshSession, "no public key matches")
            SSH_AUTH_PARTIAL -> handleAuthenticationFailure(_sshSession, "additional authentication required")
            SSH_AUTH_SUCCESS -> println("User authenticated")
        }
        setupSftpSession()
    }

    private fun authenticateManually(
        user: String,
        publicKey: String,
        privateKey: String
    ) = memScoped {
        require(!_isClosed) { "The SshClient instance must be open" }
        val homeDir = getenv("HOME")?.toKString() ?: ""
        val tmpPublicKey = alloc<ssh_keyVar>()
        val tmpPrivateKey = alloc<ssh_keyVar>()
        ssh_pki_import_pubkey_file("$homeDir/.ssh/$publicKey", tmpPublicKey.ptr)
        ssh_userauth_try_publickey(session = _sshSession, username = user, pubkey = tmpPublicKey.value)
        ssh_pki_import_privkey_file(filename = "$homeDir/.ssh/$privateKey",
            passphrase = null,
            auth_fn = null,
            auth_data = null,
            pkey = tmpPrivateKey.ptr
        )
        authenticateWithPublicKey(user, tmpPrivateKey.value)
        ssh_key_free(tmpPrivateKey.value)
        ssh_key_free(tmpPublicKey.value)
        setupSftpSession()
    }

    private fun authenticateWithPublicKey(user: String, privateKey: ssh_key?) {
        when (ssh_userauth_publickey(session = _sshSession, username = user, privkey = privateKey)) {
            SSH_AUTH_ERROR -> handleAuthenticationFailure(_sshSession, "unknown error")
            SSH_AUTH_DENIED -> handleAuthenticationFailure(_sshSession, "no public key matches")
            SSH_AUTH_PARTIAL -> handleAuthenticationFailure(_sshSession, "additional authentication required")
            SSH_AUTH_SUCCESS -> println("User authenticated")
        }
    }

    private fun setupSftpSession() {
        _sftpSession = sftp_new(_sshSession) ?: throw IoException("Cannot create new SFTP session")
        if (sftp_init(_sftpSession) != SSH_OK) {
            throw IoException("Error initializing SFTP session (code ${sftp_get_error(_sftpSession)})")
        }
    }

    private fun handleAuthenticationFailure(sshSession: ssh_session?, info: String) {
        throw IllegalStateException("Failed to authenticate ($info): ${ssh_get_error(sshSession)?.toKString()}")
    }

    public actual fun disconnect() {
        require(!_isClosed) { "The SshClient instance must be open" }
        if (_sftpSession != null) sftp_free(_sftpSession)
        _sftpSession = null
        ssh_disconnect(_sshSession)
    }
}
