import java.util.Properties

val projectSettings = fetchProjectSettings()

group = "org.combot"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.4.31"
    `maven-publish`
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    explicitApi()
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("libssh") {
                includeDirs("/usr/include", "/usr/include/libssh")
            }

            dependencies {
                implementation("$group:combot-core:$version")
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.4.31"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("$group:combot-core:$version")
            }
        }
    }
}

data class ProjectSettings(val libVer: String, val isDevVer: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        isDevVer = (properties.getProperty("isDevVer") ?: "false").toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer)
}
