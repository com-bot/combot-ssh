package org.combot.ssh.fileManagement

import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString
import libssh.*
import org.combot.core.fileManagement.File
import org.combot.core.fileManagement.authorization.FilePermissions
import org.combot.core.fileManagement.authorization.generateMode
import org.combot.ssh.IoException
import org.combot.ssh.SshClient

@Suppress("EXPERIMENTAL_API_USAGE")
public actual class RemoteDirectory actual constructor(
    private val sshClient: SshClient,
    override val parentDirPath: String,
    override val name: String
) : SftpDirectory {
    override suspend fun isValid(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return try {
            val dir = openSftpDirectory(createFilePath(parentDirPath, name), sshClient.sftpSession)
            sftp_closedir(dir)
            true
        } catch (ex: IoException) {
            false
        }
    }

    override suspend fun size(): Long {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1L
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.size?.toLong() ?: -1L
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun changeOwnership(userId: UInt, groupId: UInt): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        if (!isValid()) throw IllegalStateException("Invalid directory")
        return if (!isValid()) {
            false
        } else {
            sftp_chown(file = createFilePath(parentDirPath, name), owner = userId, group = groupId,
                sftp = sshClient.sftpSession) == SSH_OK
        }
    }

    override suspend fun changePermissions(permissions: FilePermissions): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        if (!isValid()) throw IllegalStateException("Invalid directory")
        return if (!isValid()) {
            false
        } else {
            return sftp_chmod(sftp = sshClient.sftpSession, file = createFilePath(parentDirPath, name),
                mode = permissions.generateMode()) == SSH_OK
        }
    }

    override suspend fun copy(newFile: File): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun create(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        val result = sftp_mkdir(sftp = sshClient.sftpSession, directory = createFilePath(parentDirPath, name),
            mode = defaultDirectoryPermissions().generateMode()) == SSH_OK
        changePermissions(defaultDirectoryPermissions())
        return result
    }

    override suspend fun delete(): Boolean {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        return sftp_rmdir(sshClient.sftpSession, createFilePath(parentDirPath, name)) == SSH_OK
    }

    override suspend fun fetchGroup(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.group?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchUser(): String {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = ""
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.owner?.toKString() ?: ""
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchUserId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.uid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun fetchGroupId(): Int {
        if (sshClient.isClosed) throw IllegalStateException("The SshClient instance must be open")
        var result = -1
        val attributes = sftp_stat(sshClient.sftpSession, createFilePath(parentDirPath, name))
        if (isValid()) result = attributes?.pointed?.gid?.toInt() ?: -1
        sftp_attributes_free(attributes)
        return result
    }

    override suspend fun move(newFile: File): Boolean {
        TODO("Not yet implemented")
    }
}
