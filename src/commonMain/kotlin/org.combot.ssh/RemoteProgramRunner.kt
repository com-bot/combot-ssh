package org.combot.ssh

import org.combot.core.ProgramRunner

@ExperimentalUnsignedTypes
public expect class RemoteProgramRunner(
    sshClient: SshClient,
    programName: String,
    workingDir: String = "",
    programDir: String = "",
    programArgs: Array<String> = emptyArray()
) : ProgramRunner
